module.exports = function(grunt) {

	var banner = "/*! <%= pkg.name %> v<%= pkg.version %> (<%= grunt.template.today('dd-mm-yyyy') %>) */";

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        clean: {
        	build: 'build',
        	release: 'release'
        },

        copy: {
        	build: {
        		files: [{
        			expand: true,
        			cwd: 'assets',
        			src: ['scripts/**/*.js', 'styles/**/*.css', 'vendors/**/*', 'fonts/**/*', 'images/**/*'],
        			dest: 'build'
        		}]
        	},
        	release: {
        		files: [{
        			expand: true,
        			cwd: 'assets',
        			src: ['vendors/**/*', 'fonts/**/*'],
        			dest: 'release'
        		}]
        	}
        },

        stylus: {
        	all: {
	        	files: [{
	        		expand: true,
	        		cwd: 'assets/styles',
	        		src: ['**/*.styl'],
	        		dest: 'build/styles',
        			ext: '.css'
	        	}]
        	}
        },

        uglify: {
        	options: {
        		banner: "#{banner}\n"
        	},
        	all: {
        		files: [{
        			expand: true,
        			cwd: 'build/scripts',
        			src: ['**/*.js', '!**/*.min.js'],
        			dest: 'release/scripts',
        			ext: '.js'
        		}]
        	}
        },

        uncss: {
        	release: {
        		files: {
        			'release/styles/styles.css': [
        				'build/vendors/bootstrap/dist/css/bootstrap.css',
        				'build/styles/styles.css'
        			]
        		}
        	}
        },

        cssmin: {
        	options: {
        		banner: banner
        	},
        	all: {
        		files: [{
        			expand: true,
        			cwd: 'release/styles',
        			src: ['*.css', '!*.min.css'],
        			dest: 'release/styles',
        			ext: '-built.css'
        		}]
        	}
        },

        imagemin: {
        	options: {
        		pngquant: true
        	},
        	all: {
        		files: [{
        			expand: true,
        			cwd: 'build/images',
        			src: ['**/*.{png,jpg,jpeg,gif}'],
        			dest: 'release/images'
        		}]
        	}
        },

        svgmin: {
        	all: {
        		files: [{
        			expand: true,
        			cwd: 'build/images',
        			src: ['**/*.svg'],
        			dest: 'release/images'
        		}]
        	}
        },

        express: {
        	options: {
        		port: 3000
        	},
        	dev: {
        		options: {
        			script: 'app/server.js'
        		}
        	},
        	test: {
        		options: {
        			script: 'app/server.js',
        			node_env: 'test'
        		}
        	},
        	prod: {
        		options: {
        			script: 'app/server.js',
        			node_env: 'production'
        		}
        	}
        },

        watch: {
        	options: {
        		livereload: true,
        		spawn: false
        	},
        	app: {
        		files: ['app/**/*.js'],
        		tasks: ['express:dev']
        	},
        	scripts: {
        		files: ['assets/scripts/**/*.coffee'],
        		tasks: ['newer:coffee']
        	},
        	styles: {
        		files: ['assets/styles/**/*.styl'],
        		tasks: ['newer:stylus']
        	}
        }
    });
    
    require('load-grunt-tasks')(grunt);

    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy', 'stylus', 'uglify', 'uncss', 'cssmin', 'imagemin', 'svgmin']);

    grunt.registerTask('serve', ['copy', 'stylus', 'express:dev', 'watch']);

};
