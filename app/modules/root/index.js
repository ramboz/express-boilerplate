module.exports = function(app, express) {
	'use strict';
    
    // Load module routes
    var router = require('./routes')(app, express);
	
    return router;

};
