module.exports = function(app, express) {
	'use strict';
    
    var router = express.Router();
    
	// define the home page route
	router.get('/', function(req, res, next) {
		res.render('modules/root/views/index', {}, res.renderingErrorHandler);
	});
    
    router.get('/secured', app.get('acl').middleware(), function(req, res) {
    	// whatever
    	next();
	});

    // Handle resource creation
    /*
	router.post('/', function(req, res, next){
		...
	});*/

	// Handle resource updaes
    /*
	router.put('/:id', function(req, res, next){
		...
	});*/

	// Handle resource deletion
    /*
	router.delete('/:id', function(req, res, next){
		...
	});*/
	
    return router;

};
