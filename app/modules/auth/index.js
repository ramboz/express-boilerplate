module.exports = function(app, express) {
    'use strict';
    
    var passport = app.get('passport'),
        router = express.Router();
    
    router.get('/login', function(req, res, next) {
        if (req.session.passport.user) {
            return res.redirect('/');
        }
        res.render('modules/auth/views/login', {}, res.renderingErrorHandler);
    });

    router.get('/logout', function(req, res, next) {
        req.logout();
        res.redirect('/');
    });

    if ('development' === app.get('env')) {
        require('./local')(passport, router);
    }
    
    require('./google')(passport, router);
    
    return router;

};
