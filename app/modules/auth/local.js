module.exports = function(passport, router) {
    'use strict';
    
    var LocalStrategy = require('passport-local').Strategy;

    passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        function(username, password, done) {
            if (username === 'admin' && password === 'admin') {
                return done(null, { id: username, username: username });
            }
            done({ status: 403, msg: 'Invalid user' });
        })
    );

    router.post('/local',
        passport.authenticate('local', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login'
        })
    );

};
