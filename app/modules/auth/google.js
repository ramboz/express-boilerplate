module.exports = function(passport, router) {
    'use strict';
    
    var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

    /* Google authentication strategy */
    passport.use(new GoogleStrategy({
            clientID: '736792299962-0ptr17calasfqik78t40qafvojidkf0u.apps.googleusercontent.com',
            clientSecret: 'uTi9rYTqMjO0JzXB3kVaNDfS',
            callbackURL: 'http://127.0.0.1:3000/auth/google/callback'
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                return done(null, profile);
            });
        }
    ));
    
    /* Authenticate via Google */
    var scope = [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
    ];
    router.get('/google',
        passport.authenticate('google', { scope: scope }),
        function(req, res, next) {
            // Nothing to do
        }
    );
    router.get('/google/callback/',
        passport.authenticate('google', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login'
        })
    );

};
