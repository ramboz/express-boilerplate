module.exports = function(express){
    'use strict';

    var logger = require('morgan'),
        app    = express();

	// Configure the application    
    require('./config')(app, express);

    // Load the application routes
    require('./routes')(app, express);

    return app;
}
