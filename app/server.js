module.exports = (function(){
	'use strict';

    var express = require('express'),
        app     = express();
    
    // Initialize the application
    app = require('./app')(express);

    // Start the server
    app.listen(app.get('port'), function(){
        console.log(app.get('name') + ' v' + app.get('version') + ' (' + app.get('env') + ') is running on port ' + app.get('port'));
    });

    return app;
    
})()
