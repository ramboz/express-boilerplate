module.exports = function(app, express, pkg){
    'use strict';

    var pkg = require('../../package.json');
    
    // Set some global variables
    app.set('name', pkg.name);
	app.set('port', process.env.PORT || 1337);
    app.set('version', pkg.version);

}
