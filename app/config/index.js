module.exports = function(app, express){
    'use strict';

    require('./settings')(app, express);
    require('./express')(app, express);
    require('./acl')(app, express);
    require('./authentication')(app, express);
    require('./i18n')(app, express);
    require('./xss')(app, express);

}
