module.exports = function(app, express){
    'use strict';

    var jade = require('jade'),
        xss = require('secure-filters');
    
    jade.runtime.escape = xss.html;
    
    // Make sanitization functions available to template engine
    app.locals.encodeForCss     = xss.css,      // Sanitizes CSS contexts using backslash-encoding.
    app.locals.encodeForHtml    = xss.html,     // Sanitizes HTML contexts using entity-encoding.
    app.locals.encodeForJs      = xss.js,       // Sanitizes JavaScript string contexts using backslash-encoding.
    app.locals.encodeForJsAttr  = xss.jsAttr,   // Sanitizes JavaScript string contexts in an HTML attribute using a combination of entity- and backslash-encoding.
    app.locals.encodeForJsObj   = xss.jsObj,    // Sanitizes JavaScript literals (numbers, strings, booleans, arrays, and objects) for inclusion in an HTML script context.
    app.locals.encodeForStyle   = xss.style,    // Sanitizes CSS contexts in an HTML style attribute
    app.locals.encodeForUri     = xss.uri       // Sanitizes URI contexts using percent-encoding.

}
