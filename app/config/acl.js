module.exports = function(app, express){
    'use strict';

	var path = require('path'),
        acl = require('acl');
    
    if ('development' === app.get('env')) {
        acl = new acl(new acl.memoryBackend());
    }
    else {
        //acl = new acl(new acl.redisBackend(redisClient, prefix));
    }
    
    app.set('acl', acl);

}
