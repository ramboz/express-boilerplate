module.exports = function(app, express){
    'use strict';

    var i18n = require('i18n'),
        path = require('path');
    
    // Configure languages
    i18n.configure({
        locales:['en', 'fr'],
        defaultLocale: 'en',
        cookie: app.get('name') + '_locale',
        directory: path.join(__dirname, '..', '/locales')
    });
    
    app.use(i18n.init);

    // Allow overriding locale in development (?locale=en)
    if ('development' === app.get('env')) {
        app.use(function(req, res, next){
            i18n.overrideLocaleFromQuery(req);
            next();
        });
    }

}
