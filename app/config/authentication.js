module.exports = function(app, express){
    'use strict';

    var passport = require('passport');

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });

    app.set('passport', passport);

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(function(req, res, next) {
        res.locals.user = req.session.passport.user;
        console.log(res.locals.user);
        next();
    });

}
