module.exports = function(app, express){
    'use strict';

	var path = require('path'),
        bodyParser = require('body-parser'),
        cookieParser = require('cookie-parser'),
        logger = require('morgan'),
        session = require('express-session');

    // Setup up template engine
	app.set('views', path.join(__dirname, '..'));
	app.set('view engine', 'jade');
    app.locals.basedir = app.get('views');
    app.locals.env = app.get('env');

    app.use(cookieParser('secret'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(session({
        resave: true,
        saveUninitialized: true,
        secret: 'secret'
    }))
    app.disable('x-powered-by');
    
    // Configure logging
    if ('development' === app.get('env')) {
        app.use(logger('dev'));
    }
    else {
        app.use(logger());
    }
    
    // Configure static resource serving
    if ('development' === app.get('env')) {
        app.use(express.static( path.join(__dirname, '..', '..', 'build')));
    }
    else {
        app.use(express.static( path.join(__dirname, '..', '..', 'release')));
    }

    // Make the rendering error handler available in each response to handle templating errors
    app.use(function(req, res, next){
        res.locals.baseUrl = req.baseUrl;
        res.renderingErrorHandler = function(err, html){
            if (err) {
                res.errorMessage = err;
                res.render('views/error', { status: 500, err: err}, function(err, html){
                    if (err) {
                        res.status(500).send('<pre>' + err.message + '</pre>');
                    }
                    else {
                        res.end(html);
                    }
                });
            }
            else {
                res.end(html);
            }
        };
        next();
    });

}
