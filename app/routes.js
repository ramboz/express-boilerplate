module.exports = function(app, express){
    'use strict';

    var baseUrlMiddleware = function(req, res, next) {
        res.locals.baseUrl = req.baseUrl;
        next();
    };

    // Load the root module to handle '/'
    var rootModule = require('./modules/root')(app, express);
	app.use('/', baseUrlMiddleware, rootModule);

    // Load the authentication module to handle '/auth'
    var authModule = require('./modules/auth')(app, express);
    app.use('/auth', baseUrlMiddleware, authModule);
    
    // Handle 404 errors
    app.use(function(req, res, next){
        res.status(404);
        res.render('views/error', {err: { status: 404, msg: 'Not found', stack: '' }}, res.renderingErrorHandler);
    });
    
    // Handle other errors (500, 403, etc.)
    app.use(function(err, req, res, next){
        if (!err.status) {
            err.status = err.errorCode;
        }
        res
            .status(err.status || err.errorCode)
            .render('views/error', {err: err}, res.renderingErrorHandler);
    });

}
